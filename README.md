# From zero to hero
## Provision and configure a k8s cluster completed of an example app with Ansible+Terraform+Helm, including GitLab CI linting

[![GitLab CI Pipeline Status](https://gitlab.com/bebosudo/from-zero-to-hero/badges/main/pipeline.svg)](https://gitlab.com/bebosudo/from-zero-to-hero/-/pipelines)

Goal of this project is the setup of a full k8s stack starting from the provisioning of VMs,
a brief Ansible configuration, before handing over to Terraform to setup the cluster and spawn
a sample deployment from a Helm chart.  
VMs are provisioned on DigitalOcean, but should be easy to take that part out and plug in another
component for other Cloud providers or on-premises virtualization solutions.

Author: Alberto Chiusole  
License: [MIT](LICENSE)

## Ansible setup

```
pip install ansible
ansible-galaxy collection install community.digitalocean
```

## Token setup

Create a hidden file `.tokens` in the home of this repo to store sensitive tokens:

```
$ cat .tokens
export DO_API_TOKEN=dop_v1_1234567890dead0beef1234567890
```

And add those variables to your environment with:

```
source .tokens
```

## Provisioning of resources

The ansible playbook `ansible/playbooks/provision.yml` can provision 1 master node and 2 worker
nodes on DigitalOcean with:

```
ansible-playbook ansible/provision.yml
```

This will also generate the file `ansible/hosts.yml`, which will then be used in the next
configuration steps.

Ssh into each node in `ansible/hosts.yml` manually first, then ping them with ansible to check
that the hostfile is setup correctly: you should see three nodes reply the ping:

```
ansible -i ansible/hosts.yml -m ping k8s
```

## Kubernetes cluster configuration

To configure the Kubernetes cluster with CRI-O as container runtime run:

```
ansible-playbook -i ansible/hosts.yml ansible/kubernetes.yml
```

If you have `kubectl` installed locally (installation instructions
[here](https://kubernetes.io/docs/tasks/tools/#kubectl)), you should be able to query the nodes
from your local computer by specifying the configuration file created by the playbook with:

```
$ KUBECONFIG=kubernetes.config kubectl get nodes
NAME          STATUS   ROLES           AGE   VERSION
k8s-master1   Ready    control-plane   12m   v1.25.3
k8s-worker1   Ready    <none>          12m   v1.25.3
k8s-worker2   Ready    <none>          12m   v1.25.3
```

DigitalOcean doesn't enable firewalls by default: if you adapt this to other providers you'll need
to make holes in the firewall at least for the API server listening on port 6443, and possibly
more.

## Deploy application on Kubernetes using Terraform

Deploy a sample application on the Kubernetes cluster we just built using Terraform:

```
cd terraform/
terraform init
terraform plan -out terraform.plan
```

Review the differences that the terraform step is planning on doing, once you're satisfied
apply the plan previously exported with:

```
terraform apply terraform.plan
```

The ansible playbook generated the `terraform/terraform.tfvars` file on the kubernetes master
node and copied it back into the local machine inside the terraform/ directory: this is the
reason why we don't have to teach Terraform how to reach the cluster built in the previous step.

Note that if you want to run the Terraform linter with podman on a non-Linux system that therefore
requires virtualization, you'll have to expose the paths through the VM at init time. If your
VM is already running you can stop it and create a fresh one that exposes your home and
subdirectories with the following sequence:

```
podman machine stop
podman machine rm
podman machine init --cpus=2 --disk-size=100 --memory=2048 -v $HOME:$HOME
podman machine start
```

To run the linter on the terraform directory then you can run the following from the root of your
repo:

```
podman run -it --rm -v $PWD:/data ghcr.io/terraform-linters/tflint -v /data/terraform/
```

## Cluster teardown

To teardown the nodes after you're done playing provide the inventory file generated at
provisioning time:

```
ansible-playbook -i ansible/hosts.yml ansible/teardown.yml
```

